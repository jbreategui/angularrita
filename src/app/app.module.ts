import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { MenuComponent } from './components/menu/menu.component';
import { ContentComponent } from './components/content/content.component';
import { OnboardComponent } from './components/onboard/onboard.component';
import { LoginComponent } from './components/login/login.component';


//routes
import {APP_ROUTING} from './app.routes';
import { HomeComponent } from './components/home/home.component';
import { RecogerComponent } from './components/content/recoger/recoger.component';
import { PedidoComponent } from './components/content/pedido/pedido.component';
import { EligeRitaComponent } from './components/content/pedido/elige-rita.component';
import { EligeJugoComponent } from './components/content/pedido/elige-jugo.component';
import { EligeToppingsComponent } from './components/content/pedido/elige-toppings.component';
import { EligeProductoComponent } from './components/content/recoger/elige-producto.component';
import { EscaneaCodigoQRComponent } from './components/content/pagar/escanea-codigo-qr.component';
import { PerfilComponent } from './components/content/perfil/perfil.component';
import { BuzonSugerenciasComponent } from './components/content/buzon-sugerencias/buzon-sugerencias.component';
import { RecomendarComponent } from './components/content/recomendar/recomendar.component';
import { ContactanosComponent } from './components/contactanos/contactanos.component';
import { PagarPedidoComponent } from './components/content/pedido/pagar-pedido.component';
import { PagarComponent } from './components/content/pagar/pagar.component';
import { OperacionExitosaPedidoComponent } from './components/content/pedido/operacion-exitosa-pedido.component';
import { OperacionDenegadaPedidooComponent } from './components/content/pedido/operacion-denegada-pedidoo.component';
import { OperacionExitosaComponent } from './components/content/pagar/operacion-exitosa.component';
import { OperacionDenegadaComponent } from './components/content/pagar/operacion-denegada.component';
import { RecogerResumenPedidoComponent } from './components/content/recoger/recoger-resumen-pedido.component';
import { ResumenPedidoComponent } from './components/content/pedido/resumen-pedido.component';
import { PrincipalComponent } from './components/principal/principal.component';
    
@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    MenuComponent,
    ContentComponent,
    OnboardComponent,
    LoginComponent,
    HomeComponent,
    RecogerComponent,
    PedidoComponent,
    EligeRitaComponent,
    EligeJugoComponent,
    EligeToppingsComponent,
 
    EligeProductoComponent,
     EscaneaCodigoQRComponent,
    PerfilComponent,
    BuzonSugerenciasComponent,
    RecomendarComponent,
    ContactanosComponent,
    PagarPedidoComponent,
    PagarComponent,
    OperacionExitosaPedidoComponent,
    OperacionDenegadaPedidooComponent,
    OperacionExitosaComponent,
    OperacionDenegadaComponent,
    RecogerResumenPedidoComponent,
    ResumenPedidoComponent,
    PrincipalComponent
    
    
     
     
  ],
  imports: [
    BrowserModule,
    APP_ROUTING
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
